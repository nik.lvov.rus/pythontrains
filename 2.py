from datetime import datetime

def time(func):
    def wrapper():
        start = datetime.now()
        result = func()
        print(datetime.now() - start)
        return result
    return wrapper


@time
def some_action():
    #какое то действие функции

#a = some_action()
#print(l1)