def reverse_my_list(lst):
    last_el = lst.pop()
    first_el = lst[0]
    del lst[0]
    lst.append(first_el)
    lst.insert(0,last_el)
    return lst
st = list(input().split())
print(reverse_my_list(st))